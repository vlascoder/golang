package main

import (
    "fmt"
    "./book"
)

func main () {
    var b = book.NewBook(7,12,2)
    fmt.Printf("Side %d: %v\n", 1, b.GetPagesBySide(1))
    fmt.Printf("Side %d: %v\n", 2, b.GetPagesBySide(2))
}

