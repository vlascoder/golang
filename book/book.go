package book

import (
    "fmt"
)

const debug = false

type Book struct {
    firstPage int
    lastPage int
    dummyPage int
    sheetsCnt int
    maxPage int
    sheets *[][]int
}

func NewBook(first, last, dummy int) Book {
    b := Book{first, last, dummy, 0, 0, nil}
    b.getSheetsCnt()
    b.getMaxPage()
    if debug { fmt.Printf("Book: %v\n", b) }
    return b
}

func (b *Book) getMaxPage() {
    b.maxPage = b.firstPage + 4 * b.sheetsCnt - 1
    if debug { fmt.Printf("Max page: %d\n", b.maxPage) }
}

func (b *Book) getSheetsCnt() {
    var p,n int
    p = b.lastPage - b.firstPage
    n = p / 4
    if (p > n * 4) {
        n++
    }
    b.sheetsCnt = n
    if debug { fmt.Printf("Sheets cnt: %d\n", b.sheetsCnt) }
}

func (b *Book) getSheets() {
    s := make([][]int,0)
    b.sheets = &s
    for i := 0; i < b.sheetsCnt; i++ {
        p := make([]int, 4)
        p[0] = b.firstPage + i*2 + 1
        p[1] = b.maxPage - i*2 - 1
        p[2] = b.maxPage - i*2
        p[3] = b.firstPage + i*2

        for j := 0; j < len(p); j++ {
            if (p[j] > b.lastPage || p[j] < b.firstPage) {
                p[j] = b.dummyPage
            }
        }

        s = append(s,p)
    }
    if debug { fmt.Printf("Sheets: %v\n", b.sheets) }
}

func (b *Book) GetPagesBySide(side int) ([]int) {
    if b.sheets == nil { b.getSheets() }
    s := b.sheets
    pages := make([]int, 0)

    for sheet := 0; sheet < len(*s); sheet++ {
        if (side == 1) {
            pages = append(pages, (*s)[sheet][0:2]...)
        } else {
            pages = append(pages, (*s)[sheet][2:4]...)
        }
    }

    return pages
}

